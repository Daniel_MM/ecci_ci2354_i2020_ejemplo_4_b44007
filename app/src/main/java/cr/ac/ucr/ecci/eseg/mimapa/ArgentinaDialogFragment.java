package cr.ac.ucr.ecci.eseg.mimapa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ArgentinaDialogFragment extends DialogFragment {
    private static final String ARGUMENTO_TITLE = "TITLE";
    private static final String ARGUMENTO_FULL_SNIPPET = "FULL_SNIPPET";
    private String title;
    private String fullSnippet;

    public ArgentinaDialogFragment() {
    }

    static ArgentinaDialogFragment newInstance(String title, String fullSnippet) {
        ArgentinaDialogFragment fragment = new ArgentinaDialogFragment();
        Bundle b = new Bundle();
        b.putString(ARGUMENTO_TITLE, title);
        b.putString(ARGUMENTO_FULL_SNIPPET, fullSnippet);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            title = args.getString(ARGUMENTO_TITLE);
            fullSnippet = args.getString(ARGUMENTO_FULL_SNIPPET);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(fullSnippet)
                .create();
    }
}