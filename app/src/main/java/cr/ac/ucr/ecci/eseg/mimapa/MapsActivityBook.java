package cr.ac.ucr.ecci.eseg.mimapa;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Locale;

public class MapsActivityBook extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowClickListener {
    private static final int LOCATION_REQUEST_CODE = 101;
    //
    private static final LatLng MUSEUM = new LatLng(38.8874245, -77.02200729);
    private static final LatLng SYDNEY = new LatLng(-34, 151);
    //
    private Marker markerPais;
    public static final String EXTRA_LATITUDE = "latitude";
    public static final String EXTRA_LONGITUDE = "longitude";
    //
    private Marker markerEcuador;
    private Marker markerArgentina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_book);
// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.myMap);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    protected void requestPermission(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionType}, requestCode
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length == 0
                    || grantResults[0] !=
                    PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,
                        "Unable to show location - permission required",
                        Toast.LENGTH_LONG).show();
            } else {
                SupportMapFragment mapFragment =
                        (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.myMap);
                if (mapFragment != null) {
                    mapFragment.getMapAsync(this);
                }
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this
     * case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to
     * install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            int permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permission == PackageManager.PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true);
            } else {
                requestPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        LOCATION_REQUEST_CODE);
            }
        }
        UiSettings mapSettings;

        assert googleMap != null;

        mapSettings = googleMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true); //Set Zoom Controls
        mapSettings.setAllGesturesEnabled(true); //Set All Gestures
        mapSettings.setScrollGesturesEnabled(true); //Set Scroll
        mapSettings.setTiltGesturesEnabled(true); //Set Tilt
        mapSettings.setRotateGesturesEnabled(true); //Set Rotate

        googleMap.setBuildingsEnabled(true); //3D view
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE); //Map type


        // Lista de ejemplos relacionados con mapas
        // Para probar cada uno de ellos remueva o agregue marcas de los comentarios
        CameraPosition cameraPosition;
        Marker mylocation;

        //See the compass
        mapSettings.setCompassEnabled(true); //Set Compass
        //Put maker in Sydney
        //Setting camera
        LatLng sydney = new LatLng(-34, 151);
        googleMap.addMarker(new MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney"));
        cameraPosition = new CameraPosition.Builder()
                .target(SYDNEY)
                .zoom(50)
                .bearing(70)
                .tilt(25)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Marker draggable
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .draggable(true));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Customer the marker color
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .icon(BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_ROSE)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Setting marker opacity
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .alpha(0.3f));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Flat the marker
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .flat(false));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Custom marker image
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .title("Sydney")
                .snippet("Population: 4,293,000")
                .icon(BitmapDescriptorFactory.fromResource(
                        R.drawable.common_full_open_on_phone)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Rotate marker
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .anchor(0.5f, 0.5f)
                .rotation(30.0f));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        //Add a windows info
        mylocation = googleMap.addMarker(new MarkerOptions()
                .position(SYDNEY)
                .title("Sydney")
                .snippet("Population: 4,293,000"));

        mylocation.showInfoWindow();//when the marker is showed the window info is showed

        //mylocation.hideInfoWindow(); Hide a window info
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(SYDNEY));

        LatLng mountainView = new LatLng(37.40, -122.1);

        //Move cameraPosition to Mountain View
        cameraPosition = new CameraPosition.Builder()
                .target(mountainView)
                .zoom(10)
                .bearing(70)
                .tilt(25)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // Add a marker in Mountain View
        googleMap.addMarker(new MarkerOptions()
                .position(mountainView)
                .title("Marker in Mountain View"));

        // Instantiates a new Polygon object and adds points to define a rectangle
        PolygonOptions rectOptions = new PolygonOptions()
                .add(new LatLng(37.35, -122.0),
                        new LatLng(37.45, -122.0),
                        new LatLng(37.45, -122.2),
                        new LatLng(37.35, -122.2),
                        new LatLng(37.35, -122.0));

        // Get back the mutable Polygon
        Polygon polygon = googleMap.addPolygon(rectOptions);

        //Move cameraPosition to Mountain View
        LatLng melbourne = new LatLng(-37.81319, 144.96298);

        // Add a marker in melbourne
        googleMap.addMarker(new MarkerOptions()
                .position(melbourne)
                .title("Marker in Melbourne"));
        LatLng perth = new LatLng(-31.95285, 115.85734);

        // Add a marker in melbourne
        googleMap.addMarker(new MarkerOptions()
                .position(perth)
                .title("Marker in Perth"));
        cameraPosition = new CameraPosition.Builder()
                .target(perth)
                .zoom(10)
                .bearing(70)
                .tilt(25)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // Add blue line from Perth to Melbourne
        Polyline line = googleMap.addPolyline(new PolylineOptions()
                .add(perth, melbourne)
                .width(25)
                .color(Color.BLUE)
                .geodesic(false));

        //Add an overlay to the map
        LatLng newWark = new LatLng(40.714086, -74.228697);
        googleMap.addMarker(new MarkerOptions()
                .position(newWark)
                .title("Marker in New Wark"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(newWark));
        GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(
                        R.drawable.common_full_open_on_phone))
                .position(newWark, 8600f, 6500f);

        // Add an overlay to the map, retaining a handle to the GroundOverlay object.
        GroundOverlay imageOverlay = googleMap.addGroundOverlay(newarkMap);
        //imageOverlay.remove(); // Remove overlay map

        // Iniciar una actividad cuando un marcador es presionado
        LatLng colombia = new LatLng(4.6, -74.08);
        markerPais = googleMap.addMarker(new MarkerOptions().position(colombia)
                .title("Colombia"));

        // Camara
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(colombia));

        // Listener for click on the marker
        googleMap.setOnMarkerClickListener(this);

        // Ejemplo de Drag de un marcador
        LatLng ecuador = new LatLng(-0.217, -78.51);
        markerEcuador = googleMap.addMarker(new MarkerOptions()
                .position(ecuador)
                .title("Ecuador")
                .draggable(true));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(ecuador));
        googleMap.setOnMarkerDragListener(this);

        // Ejemplo de una ventana informativa
        LatLng argentina = new LatLng(-34.6, -58.4);
        markerArgentina = googleMap.addMarker(new MarkerOptions()
                .position(argentina)
                .title("Argentina"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(argentina));
        googleMap.setOnInfoWindowClickListener(this);
    }

    // Get latitude and longitude, Send how extra to the new intent @param marker @return
    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.equals(markerPais)) {
            Intent intent = new Intent(this, MarkerDetailActivity.class);
            intent.putExtra(EXTRA_LATITUDE, marker.getPosition().latitude);
            intent.putExtra(EXTRA_LONGITUDE, marker.getPosition().longitude);
            startActivity(intent);
        }
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        if (marker.equals(markerEcuador)) {
            Toast.makeText(this, "START", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        if (marker.equals(markerEcuador)) {
            String newTitle = String.format(Locale.getDefault(),
                    getString(R.string.marker_detail_latlng),
                    marker.getPosition().latitude,
                    marker.getPosition().longitude);
            setTitle(newTitle);
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        if (marker.equals(markerEcuador)) {
            Toast.makeText(this, "END", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (marker.equals(markerArgentina)) {
            ArgentinaDialogFragment.newInstance(marker.getTitle(), getString(R.string.argentina_full_snippet)).show(getSupportFragmentManager(), null);
        }
    }
}